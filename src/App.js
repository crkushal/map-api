import React, { Component } from 'react';
import './App.css';
import Weather from './components/Weather';
import Header from './components/Header';

class App extends Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <Header/>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <Weather/>
          </div>
        </div>
      </div>
    );
  }
}
export default App;
