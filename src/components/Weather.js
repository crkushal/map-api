import React ,{Component} from 'react';
import {sendLocation} from '../actions/actionCreators';
import {connect} from 'react-redux';
import MapContainer from '../containers/MapContainer';

class Weather extends Component{

   componentWillMount = () => {
      this.getLocation();
   }

   getLocation = () => {
      if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition(this.showPosition);
      }
   }
   showPosition = (position) =>{
      var points = [
         { lat: 36.1699, lng: -115.1398 },
         { lat: 40.7128, lng: -74.0060 },   
     ];
      let lat = position.coords.latitude;
      let long= position.coords.longitude;
      this.props.sendLocation(points);
   }
   render(){
      return(
         <div className="weather-data">
            <MapContainer/>
         </div>
      );
   }
}

export default connect(null,{sendLocation})(Weather);