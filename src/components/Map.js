import React ,{Component} from 'react';
import {connect} from 'react-redux';
import {Map as GoogleMap, InfoWindow, Marker,GoogleApiWrapper} from 'google-maps-react';
import { ClipLoader } from 'react-spinners';

class Map extends Component{

   constructor(props){
      super(props);
      this.state = {
         isLoading:false,
         selectedPlace: '',
         activeMarker:undefined,
         showingInfoWindow: false
      }
   }

   onMarkerClicked = (props, marker, e) =>{
      this.setState({
        selectedPlace: props,
        activeMarker: marker,
        showingInfoWindow: true
      });
      
   }

   _renderMap = () => {
      let points = this.props[0];
      console.log(points)
      for (let i = 0; i < points.length; i++) {
         var data = points[i];
         var latitude = data.lat;
         var longitude = data.lng;
      }
      if(true){
         return(
            <div>
               <GoogleMap
                  google={this.props.google}
                  initialCenter={{
                  lat: 37.0902,
                  lng: -95.7129
                  }}
                  zoom={5}
                  onClick={this.onMapClicked}
               >
                  <Marker
                     name={'Genese'}
                     position={{lat:latitude, lng:longitude}}
                     onClick ={this.onMarkerClicked}
                     />
                     <InfoWindow
                        marker={this.state.activeMarker}
                        visible={this.state.showingInfoWindow}>
                           <div>
                           <h1>{this.state.selectedPlace.name}</h1>
                           </div>
                     </InfoWindow>
               </GoogleMap>
            </div>
         );
      }
      else{
         return(
            <ClipLoader
            style={{position:'center'}}
            color={'#123abc'} 
            loading={this.state.loading} 
          />
         );
      }
   }

  render(){
     return(this._renderMap());
  }
}
export default GoogleApiWrapper(
   (props) => ({
     apiKey: props.apiKey,
     language: props.language,
   }
 ))(Map);