import React ,{Component} from 'react';
import {connect} from 'react-redux';
import Map from '../components/Map';

class MapContainer extends Component{



  render(){
     const {data} = this.props;
     return(
      <div>
        <Map {...data}/>
      </div>
     );
  }
}
const mapStateToProps = (state)=> {
   return{
      data:state.data
   }
}
export default connect(mapStateToProps)(MapContainer);