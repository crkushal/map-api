const initialState = [];

export const weatherReducer = (state=initialState ,action)=>{
   switch (action.type){
      case "LOAD_WEATHER_DATA":
        return[...state,action.points]
      default:
         return state;
   }
}